#!/usr/bin/env zsh

# TODO remove manganelo from the directory names

echoerr() { echo "$@" 1>&2; }

helpStr="Usage: manganelo.sh 'url' options
--help | -h: print this help string
--no-gohome | -n: delete gohome.png and logo-chap.png
--downloads | -d: place output dirs into ~/Downloads

Make sure the URL begins with 'http'.
The files are best viewed with the command \`ls -v | sxiv -\`."

mangaURL=
nohome=

foreach str in $@; do
    case "$str" in
    '--no-gohome' | '-n')
        nohome=1
        ;;
    http*)
        mangaURL="$str"
        ;;
    '--help' | '-h')
        echo "$helpStr"
        exit 0
        ;;
    '--downloads' | '-d')
        cd "$HOME/Downloads"
        ;;
    *)
        echoerr "manganelo.sh: do not recognize $str"
        exit 2
        ;;
    esac
done

[ -z mangaURL ] && echoerr "Error: no manga URL."

tmpFile='/tmp/manganelo.html'

wget -O - "$mangaURL" > "$tmpFile"

curUrl="$mangaURL"

chapter=1
while [ ! -z $curUrl ]; do
    local title=$(sed -n 's/<title>\(.*\)<\/title>/\1/Ip' "$tmpFile")
    (cd "$title" &>/dev/null || mkdir "$title") && cd "$title" || (mkdir chptr"$chapter" && cd chptr"$chapter")
    wget -nd -H -p -A jpg,jpeg,png,gif -e robots=off "$curUrl"
    cd ..
    
    curUrl=$(grep 'class="navi-change-chapter-btn-next a-h"' "$tmpFile"\
    | head -1 | sed -r \
    's/^.+class="navi-change-chapter-btn-next a-h"\s+href="([^"]+)".+$/\1/')
    [ -z $curUrl ] && break

    wget -O - "$curUrl" > "$tmpFile"
    chapter=$((chapter+1))
done

rm "$tmpFile"

[ ! -z nohome ] && rm **/gohome.png **/logo-chap.png