#!/usr/bin/env zsh

# TODO test curl -O setting
# TODO cut the s off of the last datechar when the wait number is 1

# Globals (set defaults here)
dl_path="$HOME/Downloads"
qual='720p'
int='5m'
program='wget'
start_as='1'


helpStr='animehelper.sh: Wrapper around animedownloader to download entire seasons of 
shows.
Required Arguments:
    --url     | -u: The URL

Optional Arguments:
    --help    | -h: Print this help message
    --name    | -n: The name of the anime to download (This names the download 
                    files) Defaults to whatever the downloader names the files
    --qual    | -q: Show resolution (likely 480p|720p|1080p). Defaults to 720p
    --dl-path | -d: Download path. Defaults to ~/Downloads
    --program | -p: The program to use to download. Defaults to wget
    --int     | -i: The time interval to wait between downloads. Defaults to
                    5 minutes. See sleep(1)
    --start-as| -s: Start at nth episode (must be <= 1). Defaults to 1'

# Error echoing function
echoerr() { echo '\x1b[31m animehelper Error: ' '\x1b[0m' "$@"  1>&2; }

while [ -n "$1" ]; do
    case "$1" in
        '--url' | '-u')
            shift
            url="$1"
            ;;
        '--help' | '-h')
            echo "$helpStr" && exit 0
            ;;
        '--dl-path' | '-p')
            shift
            dl_path="$1"
            ;;
        '--qual' | '-q')
            shift
            qual="$1"
            ;;
        '--name' | '-n')
            shift
            name="$1"
            ;;
        '--interval' | '-i')
            shift
            int="$1"
            ;;
        '--program' | '-p')
            shift
            program="$1"
            ;;
        '--start-as' | '-s')
            shift
            start_as="$1"
            ;;
        *)
            echoerr "do not recognize option $1" && exit 1
            ;;            
    esac
    shift
done

## Error checking

# Make sure everything is set

[ -z "$url" ] && echoerr "URL not set" && exit 1
[ -z "$qual" ] && echoerr "Quality not set" && exit 1
[ -z "$dl_path" ] && echoerr "Download path not set" && exit 1
[ -z "$program" ] && echoerr "Program not set" && exit 1
[ -z "$int" ] && echoerr "Interval not set" && exit 1
[ -z "$start_as" ] && echoerr "Starting index not set" && exit 1

[[ ! $start_as =~ '^[0-9]+$' ]] && echoerr "Nth index $start_as does not exist" && exit 1

[ "$start_as" -lt 1 ] && echoerr "Nth episode does not exist" \
    && exit 1

# Make sure program is wget or curl
([ "$program" = 'wget' ] || [ "$program" = 'curl' ]) \
    || (echoerr "$program needs to be wget or curl" && exit 1)

# make sure the download directory exists
stat "$dl_path" &>/dev/null \
     || (echoerr "The download directory \'$dl_path\' does not exist" && exit 1)
# Make sure we have permission to write to the directory
(cd "$dl_path" && touch "$RANDOM") \
    || echoerr "Do not have permissions to write to the output directory \'$dl_path\'"

# Gets the next time a download will occur TODO test
converttime() {
    # sleep(1) time to date(1) time
    local timechar="$(echo $int | sed -e 's/\(^.*\)\(.$\)/\2/')"
    local intsize=${#int}
    local timestr=${int:0:$(expr $intsize - 1)}
    local datechar=

    case "$timechar" in
        'm')
            datechar='Minutes'
            ;;
        's')
            datechar='Seconds'
            ;;
        'h')
            datechar='Hours'
            ;;
        'd')
            datechar='Days'
            ;;
        *)
            echoerr "Do not recognize the format \"$timechar\""
            return 1
            ;;
    esac
    echo "$timestr $datechar"
}

getnextdatetime() {
    date -d +"$1" +"%A, %d of %B at %I:%M %p" 2> /dev/null \
        || (echoerr "Time string $int is incorrect." && exit 1)
}


episodes=($(anime dl -u "$url" -q "$qual"))

[ $? -ne 0 ] && echoerr "anime failed to fetch the file locations" && exit 2

for ((i = 1; i < $start_as; i++)); do
    [ ${#episodes[@]} -eq 0 ] \
        &&  echoerr "Nth episode is out of range" && exit 1
    shift episodes
done

animesleep() {
    wait_for="$(converttime)"
    echo '\x1b[94m'"Sleeping for $wait_for. The next download will begin at $(getnextdatetime $wait_for)" '\x1b[0m'
    sleep "$int"
}


video_index=$start_as
index_curr=1

# For setting the output file extensions (only used if name is set)
outfiles=()
fileexts=()

genfileext() {
    [ -z "$name" ] && return
    local the_name="$name$video_index"
    local foutput="$(file $the_name)"
    local result=$((echo "$foutput" | grep 'mp4' -i -q && echo 'mp4') || \
                    (echo "$foutput" | grep 'Matroska' -i -q && echo 'mkv'))
    if [ ! -z $result ]; then
        outfiles+=("$the_name")
        fileexts+=("$result")
    else
        echo "animehelper Warning: Could not get file extension of file $the_name"
    fi
}

setfileexts() {
    local sizeofarrs=${#outfiles[@]}
    for ((i = 1; i < $sizeofarrs; i++)); do
        local outfile=$outfiles[$i]
        local curext=$fileexts[$i]

        mv -v "$outfile" "$outfile$curext"
    done
}

# Main loop
foreach ep in $episodes; do
    local program_ops="$([ ! -z "$name" ] && echo -O$name$video_index)"
    "$program" "$program_ops" "$ep"
    [ $index_curr -ne "${#episodes[@]}" ] && animesleep
    genfileext
    index_curr=$((index_curr+1))
    video_index=$((video_index+1))
done 

[ ! -z "$name" ] && setfileexts
